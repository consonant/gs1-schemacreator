const fs = require('fs/promises');
const xml2js = require('xml2js');
const createSchema = require('./createSchema');

const filename = __dirname + '/GS1_Combined_01062020_EN_no_headers.xml';

const knex = require('knex')({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        port: '30000',
        user: 'gs1',
        password: 'gs1',
        database: 'gs1'
    },
    acquireConnectionTimeout: 10000000
});

const parser = new xml2js.Parser();

let segmentsCount = 0;
let familiesCount = 0;
let classesCount = 0;
let bricksCount = 0;
let attributesCount = 0;
let valuesCount = 0;
let avrelCount = 0;


async function insertData() {
    await createSchema(knex);
    let file = await fs.readFile(filename);
    let parsedObj = await parser.parseStringPromise(file);

    return Promise.all(parsedObj.root.segment.map(segment => {

    segmentsCount++;
    return knex('segment').insert({
        segment_code: segment.$.code,
        segment_definition: segment.$.definition,
        segment_text: segment.$.text,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString()
    })
        .returning('id')
        .then(segment_id => {
            console.log('segment id:', segment_id)

            return Promise.all(segment.family.map(family => {
                familiesCount++;
                return knex('family').insert({
                    segment_id: segment_id[0],
                    family_code: family.$.code,
                    family_definition: family.$.definition,
                    family_text: family.$.text,
                    created_at: new Date().toISOString(),
                    updated_at: new Date().toISOString()
                })
                    .returning('id')
                    .then(family_id => {
                        console.log(family_id)
                        if (!family['class']) return;

                        return Promise.all(family['class'].map(classs => {
                            classesCount++;
                            // Note: "class" is a reserved keyword, using "classs" instead
                            return knex('class').insert({
                                family_id: family_id[0],
                                class_code: classs.$.code,
                                class_definition: classs.$.definition,
                                class_text: classs.$.text,
                                created_at: new Date().toISOString(),
                                updated_at: new Date().toISOString()
                            })
                                .returning('id')
                                .then(classs_id => {
                                    if (!classs.brick) return;

                                    return Promise.all(classs.brick.map(brick => {
                                        bricksCount++;
                                        return knex('brick').insert({
                                            class_id: classs_id[0],
                                            brick_code: brick.$.code,
                                            brick_definition: brick.$.definition,
                                            brick_text: brick.$.text,
                                            created_at: new Date().toISOString(),
                                            updated_at: new Date().toISOString()
                                        })
                                            .returning('id')
                                            .then(brick_id => {
                                                if (!brick.attType) return;

                                                return Promise.all(brick.attType.map(attType => {
                                                    attributesCount++;
                                                    return knex('attribute').insert({
                                                        attribute_code: attType.$.code,
                                                        attribute_definition: attType.$.definition,
                                                        attribute_text: attType.$.text,
                                                        created_at: new Date().toISOString(),
                                                        updated_at: new Date().toISOString()
                                                    })
                                                        .returning('id')
                                                        // UPSERT postgresSQL
                                                        // if the attribute description is already present, do nothing
                                                        // This is because it is possible for different bricks to have the same attribute,
                                                        // not necessarly with the same set of attrValues allowed
                                                        .onConflict('attribute_code').ignore()
                                                        .then(() => {
                                                            return knex.select('id').from('attribute').where('attribute_code', '=', attType.$.code)
                                                        })
                                                        .then(attribute_id => {
                                                            if (!attType.attValue) return;

                                                            return Promise.all(attType.attValue.map(attValue => {
                                                                console.log('attribute******** ', attribute_id[0].id);
                                                                valuesCount++;
                                                                return knex('value').insert({
                                                                    value_code: attValue.$.code,
                                                                    value_definition: attValue.$.definition,
                                                                    value_text: attValue.$.text,
                                                                    created_at: new Date().toISOString(),
                                                                    updated_at: new Date().toISOString()
                                                                })
                                                                    // UPSERT postgresSQL
                                                                    .onConflict('value_code').ignore()
                                                                    .then(() => {
                                                                        // ensure to have a value_id even if the
                                                                        // previous query was silently dropped due to
                                                                        // value already present
                                                                        return knex.select('id').from('value').where('value_code', '=', attValue.$.code);
                                                                    })
                                                                    .then(value_id => {
                                                                        console.log('value******** ', value_id[0].id);
                                                                        avrelCount++;
                                                                        return knex('brick_attribute_value').insert({
                                                                            brick_id: brick_id[0],
                                                                            // this id is coming as a result from the select query, so the structure is different
                                                                            attribute_id: attribute_id[0].id,
                                                                            // this id is coming as a result from the select query, so the structure is different
                                                                            value_id: value_id[0].id,
                                                                            created_at: new Date().toISOString(),
                                                                            updated_at: new Date().toISOString()
                                                                        });

                                                                    });
                                                            }));
                                                        });
                                                }));

                                            });
                                    }));

                                });

                        }));

                    });
            }));
        });
    }));
}

function displayProg(){
    console.log('Inserted ', segmentsCount, ' segments');
    console.log('Inserted ', familiesCount, ' families');
    console.log('Inserted ', classesCount, ' classes');
    console.log('Inserted ', bricksCount, ' bricks');
    console.log('Inserted ', avrelCount, '  brick-attribute-value relations');
}
/*
let interval = setInterval(() => {
displayProg();

}, 2000)
*/
insertData().then(() => {
    //clearInterval(interval);
    console.log('Insert finished')
    displayProg();
    process.kill(process.pid, 'SIGTERM')
});

process.on('SIGTERM', () => {
    knex.destroy();
    console.log('bye');
});