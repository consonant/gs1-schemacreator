# gs1-schemecreator

> Create the schema and populate the tables with the data from an xml gs1 specification file


## Getting Started

### Prerequisites 


- [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed globally.
- A working postgresSQL database listening on a socket (to create one on the fly with docker, see below)


### Installation

2. Clone the repository
3. Install your dependencies

    ```
    cd path/to/gs1-schemacreator
    npm install
    ```

### PostgresSQL configuration with docker
- Make sure to have [docker](https://docs.docker.com/engine/install/) installed.
1. Pull postgres sql
    ```
    docker pull postgres
    ```
2. Start the container
    ```
    docker run --name <container name> -p 5432:5432 -e POSTGRES_PASSWORD=<password> -e POSTGRES_USER=<user> -e POSTGRES_DB=<db> -v <local empty folder>:/var/lib/postgresql/data -d postgres
    ```
    Substitute \<container name\> with a name for the container (must not have already been used for another container), \<user\> and \<password\> respectively with the new postgres username and password, and \<local folder\> with a folder on the host. Eg.
    ```
    docker run --name pg-gs1 -p 30000:5432 -e POSTGRES_PASSWORD=gs1 -e POSTGRES_USER=gs1 -e POSTGRES_DB=gs1 -v ~/data/pg-gs1:/var/lib/postgresql/data -d postgres
    ```
    Note:  the option -p 30000:5432 is mapping port 5432 on the container to port 30000 on the host.

    In index.js add the connection information
    ```
    let knex = require('knex')({
    client: 'pg',
    connection: {
        host: <host>,
        port: <port>,
        user: <user>,
        password: <password>,
        database: <db>
        }
    });
    ```
    Eg.: 
    ```
    let knex = require('knex')({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        port: '30000',
        user: 'gs1',
        password: 'gs1',
        database: 'gs1'
        }
    });
    ```
### PostgresSQL database reset
Enter the shell inside the container in interactive mode
```
docker exec -it <container-name> bash
```
Enter the postgres prompt:

```
psql -U gs1
```

Delete all tables:

```
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
```

Note: this will delete every table in the public schema (the default one) in the selected database!




### Start the app
    npm start
