const knex = require('knex')({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        port: '30000',
        user: 'gs1',
        password: 'gs1',
        database: 'gs1'
    },
    acquireConnectionTimeout: 10000000
});
let completed = 0;
async function addProducts(db){
    let queries = [];
    for(let i = 0; i < 1000000; i++){
        queries.push(db.raw('\
        WITH new_prod_brick as\
        (SELECT id FROM brick ORDER BY RANDOM() LIMIT 1),\
            new_prod_attrs AS\
                (SELECT attribute_id from brick_attribute_value where brick_id IN(select * from new_prod_brick)),\
                new_prod_all_possible_values AS\
                    (SELECT id, ROW_NUMBER() OVER(PARTITION BY attribute_id ORDER BY RANDOM()) AS RowNum\
             FROM brick_attribute_value WHERE brick_id IN(select * from new_prod_brick)),\
                    new_prod_random_values as\
                    (SELECT id from new_prod_all_possible_values where RowNum = 1),\
        new_product_id as\
            (INSERT INTO product(product_name, brick_id, product_definition) SELECT  \'testname\', (select * from new_prod_brick), \'testdescription\' RETURNING id)\
        INSERT INTO product_attribute_value(product_id, brick_attribute_value_id) SELECT new_product_id.id, new_prod_random_values.id from new_product_id, new_prod_random_values;\
        '
        )
        .then(() => {
            completed++;
        })
        
        );
    }
    return Promise.all(queries);

}

let interval = setInterval(() => {
    console.log('Executed queries: ', completed);
}, 2000);

addProducts(knex).then(() => {
    console.log(completed, ' products added');
    process.kill(process.pid, 'SIGTERM')

});

process.on('SIGTERM', () => {
    knex.destroy();
    clearInterval(interval);
    console.log('bye');
});