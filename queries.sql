/************************************
********* PRODUCT INSERTION *********
************************************/


/*
Create a random product, assign it to a random brick, select a random value for every
attribute of the brick.
Useful for testing purposes
*/

WITH new_prod_brick AS
   (
      SELECT id
      FROM brick 
      ORDER BY RANDOM() 
      LIMIT 1
   ),
   new_prod_attrs AS
   (
      SELECT attribute_id 
      FROM brick_attribute_value
      WHERE brick_id 
      IN ( SELECT * FROM new_prod_brick )
   ),
new_prod_all_possible_values AS
   (
      SELECT id,
      ROW_NUMBER() OVER
      (
         PARTITION BY attribute_id
         ORDER BY RANDOM()
      ) 
      AS RowNum
      FROM brick_attribute_value 
      WHERE brick_id 
      IN (SELECT * FROM new_prod_brick)
   ),
   new_prod_random_values AS
   (
      SELECT id 
      FROM new_prod_all_possible_values 
      WHERE RowNum = 1
   ),
   new_product_id AS                                           
   (
      INSERT INTO product(product_name, brick_id, product_definition)
      SELECT  'test2', ( SELECT * FROM new_prod_brick), 'testdescription'
      RETURNING id
   )
INSERT INTO product_attribute_value(product_id, brick_attribute_value_id)
SELECT new_product_id.id , new_prod_random_values.id
FROM new_product_id, new_prod_random_values;

/************************************
************ BRICK INFO *************
************************************/

/* Select all attributes of a brick from its code
*/
SELECT attribute_text
FROM attribute
WHERE attribute.id
IN 
   ( SELECT attribute_id 
      FROM brick_attribute_value 
      WHERE brick_id 
      IN 
         (  SELECT id 
            FROM brick 
            WHERE brick_code = 10000275
         )     
      );

/* Select all attributes of a brick from its id
*/
SELECT attribute_text
FROM attribute
WHERE attribute.id
IN 
      ( SELECT attribute_id 
        FROM brick_attribute_value 
        WHERE id = 616     
       );

/* Select all values of an attribute for a brick */
SELECT id, value_text
FROM value 
WHERE value.id IN 
(  SELECT value_id 
   FROM brick_attribute_value 
   WHERE attribute_id 
   IN 
   (  SELECT id
      FROM attribute 
      WHERE attribute_text
      LIKE 'Method of Production' )
   AND  brick_id 
   IN 
   (  SELECT id
      FROM brick 
      WHERE brick_code = 10000111 
   )
);
/* Select all possible attributes-values for a brick */
SELECT attribute_code, attribute_text, value_text
FROM brick_attribute_value
JOIN attribute
   ON attribute.id = brick_attribute_value.attribute_id
JOIN value
   ON value.id = brick_attribute_value.value_id
WHERE brick_attribute_value.brick_id
IN (
   SELECT id 
   FROM brick 
   WHERE brick_code = 10000111
   )
ORDER BY attribute_code;


/*
Complete hierarchy of a brick (implicit join syntax)
*/
SELECT segment_text, family_text, class_text, brick_text
FROM segment, family, class, brick
WHERE brick.class_id = class.id
AND class.family_id = family.id
AND family.segment_id = segment.id
AND brick_code = 10001674
;

/*
Complete hierarchy of a brick (explicit join syntax)
*/
SELECT segment_text, family_text, class_text, brick_text
FROM brick
JOIN class ON brick.class_id = class.id
JOIN family ON class.family_id = family.id
JOIN segment ON family.segment_id = segment.id
WHERE brick_code = 10001674
;

/************************************
*********** PRODUCT INFO ************
************************************/


/* Select all the attributes of a product from its id
*/

SELECT attribute_text 
FROM attribute 
JOIN brick_attribute_value 
ON  attribute_id = attribute.id 
   AND  brick_attribute_value.id IN
   (
      SELECT brick_attribute_value_id 
      FROM product_attribute_value
      WHERE product_id = 456789
   );

/* Select all the attributes-values of a product from its id
*/

WITH attrvalueid AS
(
   SELECT brick_attribute_value_id 
   FROM product_attribute_value
   WHERE product_id = 678901
)
SELECT attribute_text, value_text
FROM attribute 
JOIN brick_attribute_value 
ON  attribute_id = attribute.id 
   AND  brick_attribute_value.id 
   IN (SELECT * FROM attrvalueid)
JOIN value
ON value_id = value.id
   AND brick_attribute_value.id 
   IN (SELECT * FROM attrvalueid)
;

/* Select all the attributes-values of a product from its name
*/

WITH attrvalueid AS
(
   SELECT brick_attribute_value_id 
   FROM product_attribute_value
   WHERE product_id
   IN
   (
      SELECT id FROM product WHERE product_name LIKE 'test2' LIMIT 1
   )
)
SELECT attribute_text, value_text
FROM attribute 
JOIN brick_attribute_value 
ON  attribute_id = attribute.id 
   AND  brick_attribute_value.id 
   IN (SELECT * FROM attrvalueid)
JOIN value
ON value_id = value.id
   AND brick_attribute_value.id 
   IN (SELECT * FROM attrvalueid)
;



/************************************
*********** GENERAL INFO ************
***********************************
*/

/*
List of bricks that use an attribute from code
*/
SELECT brick_text, attribute_text
FROM attribute, brick
WHERE brick.id 
IN (SELECT brick_id from brick_attribute_value where attribute_id in (select id from attribute where attribute_code = 36))
;
/*
List of bricks that use an attribute from id
*/
SELECT brick_text, attribute_text
FROM attribute, brick
WHERE brick.id 
IN (SELECT brick_id from brick_attribute_value where attribute_id  = 36)
AND attribute.id = 36
;
/*All families in a segment from its code
*/
SELECT family_code, family_text
FROM family
WHERE family.segment_id
IN (
   SELECT id
   FROM segment
   WHERE segment_code = 70000000
   )
;


SELECT brick_text, attribute_text,
COUNT(*) OVER
(
   PARTITION BY brick_id, attribute_id
) 
AS RowNum
from brick_attribute_value, brick, attribute
where brick_id = brick.id AND attribute_id = attribute.id
Order by RowNum DESC;

SELECT attribute_text, value_text from attribute, brick, value, brick_attribute_value
where attribute_id IN (select id from attribute where attribute_text LIKE 'Origin of Wine')
AND attribute_id = attribute.id
AND value_id = value.id
and brick_id = brick.id
and brick_id in (select id from brick where brick_text LIKE '%Wine -%' limit 1)
;

/************************************
************ STATISTICS *************
************************************/


/*
Order segments by the number of families they have (works on every db)
The same is for families-classes , classes-bricks
*/

WITH family_partitioned AS
(
   SELECT id,
   ROW_NUMBER() OVER
   (
      PARTITION BY segment_id
   )
   AS rowpartitionIndex
   FROM
   family
),
segment_rowcount AS
(
   select id,
   COUNT(*) OVER
   (
      PARTITION BY segment_id
   )
   AS rowcount
   from family
)
SELECT segment_text, rowcount
from segment, family, segment_rowcount
where segment_id = segment.id
and family.id IN ( SELECT id from family_partitioned where
   rowpartitionIndex = 1)
and family.id = segment_rowcount.id
Order by rowcount DESC;


/*
Order segments by the number of families they have (works only on postgres)
The same is for families-classes , classes-bricks

*/

SELECT * FROM (
SELECT DISTINCT ON (segment_id) segment_text,
COUNT(*) OVER
(
   PARTITION BY segment_id
)
AS number_of_families
FROM family, segment
WHERE segment_id = segment.id
) AS q ORDER BY number_of_families DESC;


/*
Order segments by their bricks (works only on postgres)
*/
SELECT * FROM (
SELECT DISTINCT ON (segment_id) segment_text,
   COUNT(*) OVER
   (
      PARTITION BY segment_id
   ) as brick_count
FROM brick
JOIN class ON brick.class_id = class.id
JOIN family ON class.family_id = family.id
JOIN segment ON family.segment_id = segment.id
) AS q ORDER BY brick_count DESC;

/*
Order segments by the number of attribute-values pairs they have. Works only on Postgres.
*/
SELECT * FROM (
   SELECT Distinct on (segment.id) segment_text, 
      COUNT(*) OVER
      (
         PARTITION BY segment_id
      ) as attribute_value_count
   FROM brick_attribute_value
   JOIN attribute
      ON attribute.id = brick_attribute_value.attribute_id
   JOIN value
      ON value.id = brick_attribute_value.value_id

   JOIN brick ON brick_attribute_value.brick_id = brick.id
   JOIN class ON brick.class_id = class.id
   JOIN family ON class.family_id = family.id
   JOIN segment ON family.segment_id = segment.id
) as q ORDER BY attribute_value_count DESC;
