module.exports = async function (db){
await db.schema.hasTable('segment').then(exists => {
        if (!exists) {
            return db.schema.createTable('segment', table => {
                table.increments('id');
                table.integer('segment_code')
                    .unique();
                table.string('segment_definition', 6000)
                table.string('segment_text', 6000)
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created segment table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating segment table', e)); // eslint-disable-line no-console
        }
    })
.catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console

await db.schema.hasTable('family').then(exists => {
        if (!exists) {
            return db.schema.createTable('family', table => {
                table.increments('id')
                    .primary();
                table.integer('segment_id')
                    .unsigned()
                    .references('id')
                    .inTable('segment')
                    .onDelete('CASCADE')
                    .index()

                table.integer('family_code')
                    .unique();
                table.string('family_definition', 6000)
                table.string('family_text', 6000)
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created family table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating family table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console


await db.schema.hasTable('class').then(exists => {
        if (!exists) {
            return db.schema.createTable('class', table => {
                table.increments('id')
                    .primary();
                table.integer('family_id')
                    .unsigned()
                    .references('id')
                    .inTable('family')
                    .onDelete('CASCADE')
                    .index()

                table.integer('class_code')
                    .unique();
                table.string('class_definition', 6000)
                table.string('class_text', 6000)
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created class table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating class table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console

await db.schema.hasTable('brick').then(exists => {
        if (!exists) {
            return db.schema.createTable('brick', table => {
                table.increments('id')
                    .primary();
                table.integer('class_id')
                    .unsigned()
                    .references('id')
                    .inTable('class')
                    .onDelete('CASCADE')
                    .index()

                table.integer('brick_code')
                    .unique();
                table.string('brick_definition', 6000)
                table.string('brick_text', 6000)
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created brick table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating brick table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console


await    db.schema.hasTable('attribute').then(exists => {
        if (!exists) {
            return db.schema.createTable('attribute', table => {
                table.increments('id').primary();
                table.integer('attribute_code')
                    .unique();
                table.string('attribute_definition', 6000)
                table.string('attribute_text', 6000)

                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created attribute table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating attribute table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console

 await   db.schema.hasTable('value').then(exists => {
        if (!exists) {
            return db.schema.createTable('value', table => {
                table.increments('id').primary();
                table.integer('value_code')
                    .unique();
                table.string('value_definition', 6000)
                table.string('value_text', 6000)
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created value table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating value table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console


 await   db.schema.hasTable('brick_attribute_value').then(exists => {
        if (!exists) {
            return db.schema.createTable('brick_attribute_value', table => {
                table.increments('id')
                    .primary();
                table.integer('brick_id')
                    .unsigned()
                    .references('id')
                    .inTable('brick')
                    .onDelete('CASCADE')
                    .index()
                table.integer('attribute_id')
                    .unsigned()
                    .references('id')
                    .inTable('attribute')
                    .onDelete('CASCADE')
                    .index()
                table.integer('value_id')
                    .unsigned()
                    .references('id')
                    .inTable('value')
                    .onDelete('CASCADE')
                    .index()

                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created brick_attribute_value table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating brick_attribute_value table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console


 await   db.schema.hasTable('product').then(exists => {
        if (!exists) {
            return db.schema.createTable('product', table => {
                table.increments('id').primary();
                table.string('product_name', 6000);
                table.integer('brick_id')
                    .unsigned()
                    .references('id')
                    .inTable('brick')
                    .onDelete('CASCADE')
                    .index()
                table.string('product_definition', 6000);
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created product table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating product table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console



await    db.schema.hasTable('product_attribute_value').then(exists => {
        if (!exists) {
            return db.schema.createTable('product_attribute_value', table => {
                table.increments('id').primary();
                table.integer('product_id')
                    .unsigned()
                    .references('id')
                    .inTable('product')
                    .onDelete('CASCADE')
                    .index()
                table.integer('brick_attribute_value_id')
                    .unsigned()
                    .references('id')
                    .inTable('brick_attribute_value')
                    // TODO constrait same brick than product.brick_id
                    .onDelete('CASCADE')
                    .index()
                table.timestamp('created_at');
                table.timestamp('updated_at');
            })
                .then(() => console.log('Created product_attribute_value table')) // eslint-disable-line no-console
                .catch(e => console.error('Error creating product_attribute_value table', e)); // eslint-disable-line no-console
        }
    })
        .catch(e => console.error('Error creating customers table', e)); // eslint-disable-line no-console

}